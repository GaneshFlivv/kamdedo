import React, {useEffect} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {Provider} from 'react-redux';
import store from './src/Utils/configureStore';
import SplashScreen from 'react-native-splash-screen';

import AppRoot from './src/App';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#54CB4A',
    accent: '#407fe3',
  },
};

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <SafeAreaProvider>
      <PaperProvider theme={theme}>
        <Provider store={store}>
          <AppRoot />
        </Provider>
      </PaperProvider>
    </SafeAreaProvider>
  );
};

export default App;
