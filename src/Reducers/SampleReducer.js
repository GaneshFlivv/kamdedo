let initialState = {
  StudentsData: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'Sample_Students_Data':
      return {
        ...state,
        StudentsData: action.sampleTotalData,
      };
    default:
      return state;
  }
};
