import {create} from 'apisauce';
import {BASE_URL} from '../Config/index';

export const URIS = {
  MYORDERS: (customerId) => `Api/Customer/MyOrders/${customerId}`,
};

const createApiClient = (baseURL = BASE_URL) => {
  const api = create({
    baseURL,
    headers: {
      Accept: 'application/json',
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json',
      app: 'consumer',
    },
    timeout: 15000,
  });

  const fetchMyOrders = (payload) => {
    return api.get(URIS.MYORDERS(payload.customerId));
  };

  // kickoff our api functions
  return {
    fetchMyOrders,
  };
};

export default {createApiClient};
