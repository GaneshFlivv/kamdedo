import Api from './index';

let apiClient = null;

const createApi = () => {
  apiClient = Api.createApiClient();
  return apiClient;
};

export default createApi;
export {createApi as ApiService};
