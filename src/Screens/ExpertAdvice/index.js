import React  from 'react'
import { View, Text, StyleSheet, ScrollView, Image , Dimensions } from 'react-native'
import { colors } from '../../Common/colors'
import Header from '../../Components/Header'

const windowWidth = Dimensions.get('window').width;

const ExpertAdviceScreen = () => {
    return (
        <View style={styles.container}>
            <Header title="Expert Advice" icon={true} />
            <View style={styles.mainView}>
                <ScrollView>
                    <View 
                    style={{
                        margin:25,
                        marginBottom:1,
                        marginTop:40,
                        flexDirection:'row',
                        justifyContent:'center'
                    }}
                    >
                    <Image source={require('../../../assets/images/expert.png')} style={{width:windowWidth-50,height:200,borderRadius:5}} />

                    </View>
                    <View style={styles.termsStyle}>
                        <Text style={styles.textStyle} >1. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                        specimen book.</Text> 
                       <Text style={styles.textStyle} >2. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                      specimen book.</Text>
                      <Text style={styles.textStyle} >3. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                      specimen book.</Text>
                      <Text style={styles.textStyle} >4. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                      specimen book.</Text>
                      <Text style={styles.textStyle} >5. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                      specimen book.</Text>
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background
    },
    mainView: {
        flex: 3,
        backgroundColor: 'white',
        marginTop: '10%',
        borderTopLeftRadius: 80,
        borderTopRightRadius: 80
    },

    textStyle: {
        fontSize: 14, color: 'black', marginTop: 10, textAlign: 'justify'
    },
    termsStyle: {
        margin: 20, marginTop: 20
    }


})
export default ExpertAdviceScreen   