import React from 'react'
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { colors } from '../../Common/colors'
import Header from '../../Components/Header'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import DatePicker from 'react-native-modern-datepicker';
import { TimingsData } from '../../Common/index'

const OrderInfoScreen = (props) => {

    const moveToInvoice = () => {
        props.navigation.navigate('InvoiceScreen')

    }

    const selectedValue = (value) => {
        console.log(value, "value >>>>");
    }

    const _renderItemTimings = (item, index) => {
        console.log(item, "item kkk");
        return (
            <Text style={{ borderWidth: 0.7, textAlign: 'center', padding: 5, borderRadius: 3, marginTop: 10,color:colors.themeColor,borderColor:colors.themeColor }}>{item.item.value}</Text>

        )
    }


    return (
        <View style={styles.container}>
        <Header title="AC Service" icon={true} />

            <ScrollView>
                <View>
                    <View style={styles.wrapper}>
                        <View style={[styles.rowspace, { margin: 15, marginBottom: 0 }]}>
                            <Text style={{ color: colors.themeColor }}>Address For Service</Text>
                            <View>
                                <Text style={styles.changeAddress}>Change Address</Text>
                            </View>
                        </View>
                        <View style={{ marginLeft: 8, flexDirection: 'row' }}>
                            <Ionicons name="location-outline" size={15} style={{ marginLeft: 5 }} color="grey" />
                            <Text style={{ fontSize: 12, marginBottom: 5 }}>8-3-228/B/1 yusufguda check post</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 15, backgroundColor: 'white',flex:1,paddingBottom:10 }}>
                        <View style={{ margin: 15 }}>
                            <Text style={{ color: colors.themeColor, fontSize: 16 }}>Service Plan</Text>
                            <Text>Schedule next session from my booking section. Don't worry we</Text>
                        </View>
                   
                    <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', }}></View>

                    <View style={{ flexDirection: 'row', margin: 5, marginRight: 8, marginLeft: 15 }}>
                        <View style={{ width: '70%', elevation: 5 ,marginTop:13,marginLeft:12}}>

                            <DatePicker
                                options={{
                                    backgroundColor: '#ffffff',
                                    textHeaderColor: '#000000',
                                    textDefaultColor: '#000000',
                                    selectedTextColor: '#fff',
                                    mainColor: colors.themeColor,
                                    textSecondaryColor: '#000000',
                                    borderColor: 'rgba(122, 146, 165, 0.1)',
                                }}
                                current="2020-07-13"
                                selected="2020-07-15"
                                mode="calendar"
                                minuteInterval={30}
                                style={{ borderRadius: 10, width: '95%', textSize: 10, height: '30%', elevation: 5 }}
                                onDateChange={(value) => selectedValue(value)}
                            />

                        </View>
                        <View style={{height:300,margin:6}}>
                          <ScrollView>
                           <FlatList
                                data={TimingsData}
                                renderItem={_renderItemTimings}
                                keyExtractor={(item) => item.id}
                            />
                           </ScrollView>
                        </View>

                    </View>
                    </View>
                </View>


                <View style={{ backgroundColor: 'white', marginTop: 25 }}>
                    <View style={[styles.rowspace, { marginLeft: 5, padding: 20 }]}>
                        <View>
                            <Text style={{ fontWeight: 'bold' }}>TOTAL</Text>
                            <TouchableOpacity>
                                <Text style={{ textDecorationLine: 'underline', color: colors.themeColor }}>View Details</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.subTotalMainView}>
                            <FontAwesome name="rupee" size={12} style={{ margin: 4 }} color={colors.themeColor} />
                            <Text style={{ fontSize: 14, color: colors.themeColor }}>1499</Text>
                        </View>

                    </View>

                </View>

                <View style={styles.subTotal}>
                    <View style={{ margin: 20 }}>
                        <View style={styles.rowspace}>
                            <Text>Item Total & Price</Text>
                            <View style={styles.subTotalMainView}>
                                <FontAwesome name="rupee" size={12} style={{ margin: 4 }} />
                                <Text style={{ fontSize: 14, color: 'grey' }}>199</Text>
                            </View>
                        </View>

                        <View style={styles.rowspace}>
                            <Text>Saftey & Suport Fee</Text>
                            <View style={styles.subTotalMainView}>
                                <FontAwesome name="rupee" size={12} style={{ margin: 4 }} />
                                <Text style={{ fontSize: 14, color: 'grey' }}>99</Text>
                            </View>
                        </View>

                        <View style={styles.rowspace}>
                            <Text>Membership Discount</Text>
                            <View style={styles.subTotalMainView}>
                                <FontAwesome name="rupee" size={12} style={{ margin: 4 }} />
                                <Text style={{ fontSize: 14, color: 'grey' }}>-100</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', marginTop: 5 }}></View>
                        <View style={[styles.rowspace, { marginTop: 25, marginLeft: 5 }]}>
                            <Text style={{ fontWeight: 'bold',color:colors.themeColor }}>TOTAL</Text>
                            <View style={styles.subTotalMainView}>
                                <FontAwesome name="rupee" size={12} style={{ margin: 4 }} color={colors.themeColor} />
                                <Text style={{ fontSize: 14, color: colors.themeColor }}>1499</Text>
                            </View>
                        </View>
                    </View>
                </View>

                <TouchableOpacity onPress={() => moveToInvoice()}>
                    <View style={{ marginBottom: 20, margin: 20 }}>
                        <Text style={styles.buttonTextStyle}>Proceed to Pay</Text>
                    </View>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background
    },
    wrapper: {

        backgroundColor: 'white',
        elevation: 5
    },
    changeAddress: {
        backgroundColor: colors.themeColor,
        padding: 5,
        color: 'white',
        textAlign: 'center',
        borderRadius: 5,
        fontSize: 12
    },
    rowspace: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    subTotal: {
        backgroundColor: 'white', marginTop: 15
    },
    subTotalMainView: {
        flexDirection: 'row',
        margin: 6, marginRight: 10,
        marginTop: 1
    }, buttonTextStyle: {
        backgroundColor: colors.themeColor, padding: 10, color: 'white', borderRadius: 5, textAlign: 'center', fontSize: 16
    },
})
export default OrderInfoScreen


