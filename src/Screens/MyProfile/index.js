import React  from 'react'
import {View ,Text ,StyleSheet, TouchableOpacity, ScrollView} from 'react-native'
import {colors }from '../../Common/colors'
import { Avatar } from 'react-native-paper';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Header from '../../Components/Header'


 const MyProfileScreen =(props)=>{
 
    const handleEditProfile =()=>{
            props.navigation.navigate('EditProfileScreen')
    }

    const hadleMoveToTerms =()=>{
        props.navigation.navigate('TermsConditionsScreen')
    }

    const hadleMoveToPrivacy =()=>{
        props.navigation.navigate('PrivacyPolicyScreen')
    }

    const hadleMoveToBookings =()=>{
        props.navigation.navigate('MyBookingsScreen')
    }
    
    
    

  return(
      <View style={styles.container}>
          <Header title="Profile" />
      <ScrollView>
     <View 
    //  style={{margin:20}}
     >
     <View style={styles.mainView}>
             <View style={{marginLeft:10,width:'20%'}}>
             <Avatar.Image size={70} source={{uri:'https://www.paychex.com/sites/default/files/styles/1200wide/public/image/2020-09/cares-act-employee-retention-credit.jpg?itok=9llOe7NR'}} />
             </View>
             <View style={styles.profileContent}>
                 <Text style={styles.textStyle}>Sam David</Text>
                 <Text>+91 9966332255</Text>
             </View>
             <View>
               <FontAwesome5 name="user-edit" size={20} color={colors.themeColor} onPress={()=> handleEditProfile()} />
             </View>
         </View>

         <TouchableOpacity onPress={()=> hadleMoveToBookings()} >
         <View style={styles.cardView}>
          <MaterialIcons name="my-library-books" size={25} color={colors.themeColor} style={{marginLeft:10,marginRight:10}} />
          <Text style={styles.cardTextStyle}>My Bookings</Text>
         </View>
         </TouchableOpacity>

         <TouchableOpacity>
         <View style={styles.cardView}>
          <MaterialIcons name="settings" size={25} color={colors.themeColor} style={{marginLeft:10,marginRight:10}} />
          <Text style={styles.cardTextStyle}>Settings</Text>
         </View>
         </TouchableOpacity>

         <TouchableOpacity onPress={()=> hadleMoveToTerms()}>
         <View style={styles.cardView}>
          <Ionicons name="newspaper" size={25} color={colors.themeColor} style={{marginLeft:10,marginRight:10}} />
          <Text style={styles.cardTextStyle}>Terms & Conditions</Text>
         </View>
         </TouchableOpacity>

         <TouchableOpacity  onPress={()=> hadleMoveToPrivacy()} >
         <View style={styles.cardView}>
          <MaterialCommunityIcons name="shield-lock" size={25} color={colors.themeColor} style={{marginLeft:10,marginRight:10}} />
          <Text style={styles.cardTextStyle}>Privacy & Policy</Text>
         </View>
         </TouchableOpacity>

         <TouchableOpacity>
         <View style={styles.cardView}>
          <MaterialIcons name="star" size={25} color={colors.themeColor} style={{marginLeft:10,marginRight:10}} />
          <Text style={styles.cardTextStyle}>Rate app on Play store</Text>
         </View>
         </TouchableOpacity>


         <TouchableOpacity>
         <View style={styles.cardView}>
          <MaterialIcons name="arrow-circle-down" size={25} color={colors.themeColor} style={{marginLeft:10,marginRight:10}} />
          <Text style={styles.cardTextStyle}>Download Kamdedo Vendor App</Text>
         </View>
         </TouchableOpacity>

         <TouchableOpacity>
         <View style={styles.cardView}>
          <MaterialIcons name="exit-to-app" size={25} color={colors.themeColor} style={{marginLeft:10,marginRight:10}} />
          <Text style={styles.cardTextStyle}>Logout</Text>
         </View>
         </TouchableOpacity>

         
     </View>

      </ScrollView>

      </View>
  )
}
const styles=StyleSheet.create({
container:{
    flex:1,
    backgroundColor:colors.background
},
mainView:{
    marginTop:'10%',
    backgroundColor:'white',
    padding:20,
    flexDirection:'row',
    borderRadius:10
},
profileContent:{
    marginLeft:20,
    marginTop:6,
    width:'60%'
},
textStyle:{
    fontSize:18, 
    color:'black'  
},
cardView:{
    backgroundColor:'white',
    marginTop:10,
    flexDirection:'row',
    padding:15,
    // borderRadius:10

},
cardTextStyle:{
    fontSize:16,
    color:'black'
}
})
export default MyProfileScreen