import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { colors } from '../../Common/colors';
import Header from '../../Components/Header'
import { Avatar } from 'react-native-paper';
import { BookingsData } from '../../Common/index'


const MyBookingsScreen = (props) => {

  const _renderItemService = (item, index) => {
    return (
      <View style={styles.mainView} >
        <View style={styles.serviceMainView}>
          <View style={{ marginLeft: 20, width: '16%' }}>
            <Avatar.Image size={60} source={{ uri: item.item.image }} />
          </View>
          <View style={{ width: '50%', marginTop: 5 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 14, color: 'black' }}>{item.item.title}</Text>
            <Text style={{ fontSize: 10 }}>{item.item.subTitle}</Text>
          </View>
          <View style={{ marginRight: 15 }}>
            <Text style={styles.status}>{item.item.Status}</Text>
          </View>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
          <Text style={{ marginRight: 30 }}>{item.item.OrderNo}</Text>
        </View>
        <View style={styles.line}>
        </View>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 20 }}>
          <View >
            <Text style={{ fontWeight: 'bold', fontSize: 14, color: 'black' }}>{item.item.ServiceName}</Text>
            <Text style={{ fontSize: 10, marginLeft: 2 }}>{item.item.subTitle}</Text>
          </View>
          <View>
            <Text style={styles.details}>View Details</Text>
          </View>
        </View>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <Header title="My Bookings" />
      <View>
        <FlatList
          data={BookingsData}
          renderItem={_renderItemService}
          keyExtractor={(item) => item.id}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background
  },
  mainView: {
    backgroundColor: 'white',
    marginTop: 5
  },
  serviceMainView: { marginTop: 10, flexDirection: 'row' },
  status: { backgroundColor: '#16B342', padding: 4, borderRadius: 3, color: 'white', paddingLeft: 10, paddingRight: 10 },
  line: { borderBottomWidth: 0.3, marginLeft: 25, marginRight: 25, marginTop: 5, borderColor: 'grey' },
  details: { borderWidth: 0.5, padding: 7, borderRadius: 3, color: colors.themeColor, borderColor: colors.themeColor }
})
export default MyBookingsScreen;
