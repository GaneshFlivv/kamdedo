import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, FlatList,TouchableOpacity } from 'react-native';
import { colors } from '../../Common/colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Searchbar } from 'react-native-paper';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { EventsData, ServiceData } from '../../Common/index'
import { Card } from 'react-native-paper';




const HomeScreen = (props) => {
  const carouselRef = React.createRef();
  const [activeIndex, setActiveIndex]=useState()

  const _renderItem = ({ item, index }) => {
    return (     
      <Card style={styles.imageCard}>
        <Card.Cover source={item.image} />
      </Card>
    );
  };

  const movetToScreen=(item)=>{
    console.log(item,"item >>.");
    props.navigation.navigate('ServiceDetailScreen',{data:item.title})
  }



  const _renderItemService = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={()=> movetToScreen(item)}>
        <View style={styles.serviceView}>
        <Image source={item.image} style={styles.serviceImage} resizeMode="contain" />
        <Text style={styles.serviceText}>{item.title}</Text>
      </View>
      </TouchableOpacity>
    );
  };


 const pagination =()=> {
   console.log('calling >>>>');
    return (
        <Pagination
          dotsLength={EventsData.length}
          activeDotIndex={activeIndex}
          containerStyle={{ backgroundColor: colors.background}}
          dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 8,
              backgroundColor:  'rgba(0, 0, 0, 0.75)'
          }}
          inactiveDotStyle={{
              // Define styles for inactive dots here
          }}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
    );
}

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={{ backgroundColor: colors.themeColor, marginTop: 20 }}>
          <View style={styles.mainView}>
            <View style={styles.row}>
              <Ionicons name="location-sharp" size={20} color={colors.white} />
              <Text style={styles.textStyle}>8-3-228/B/1 yusufguda check post ,Jubliee hills</Text>
            </View>
            <View style={styles.SearchbarStyle}>
              <Searchbar
                placeholder="Searchfor Services and Packages"
                style={{ height: 45 }}
              // onChangeText={onChangeSearch}
              // value={searchQuery}
              />
            </View>
          </View>

        </View>
        <View>
          <View style={{ margin: 15 }}>
            <Carousel
              ref={carouselRef}
              data={EventsData && EventsData}
              renderItem={_renderItem}
              sliderWidth={380}
              itemWidth={380}
              // layout={'default'}
              // // autoplay={true}
              // autoplayDelay={1000}
              // autoplayInterval={200}
              onSnapToItem={(index) => setActiveIndex(index) }
            />
              { pagination ()}
          </View>
        
        </View>
        <View style={[styles.row, styles.textView]}>
          <Image source={require('../../../assets/images/address.png')} style={{ height: 15, width: 15, marginTop: 3 }} />
          <Text style={{ marginLeft: 5, fontSize: 14 }}>Service available across Hyderabad & secundrabad.</Text>
        </View>


        <View style={styles.flatListView}>
          <FlatList
            data={ServiceData}
            renderItem={_renderItemService}
            keyExtractor={(item) => item.id}
            numColumns={4}
          />
        </View>


        <View style={styles.wrapper}>

          <View >
            <Carousel
              ref={carouselRef}
              data={EventsData && EventsData}
              renderItem={_renderItem}
              sliderWidth={380}
              itemWidth={380}
              layout={'default'}
              autoplay={true}
              autoplayDelay={100}
              autoplayInterval={200}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background
  },
  wrapper: {
    margin: 20
  },
  mainView: {
    backgroundColor: colors.themeColor, padding: 20
  },
  row: {
    flexDirection: 'row'
  },
  textView: {
    backgroundColor: 'white',
    padding: 10
  },
  textStyle: {
    color: 'white', marginLeft: 10
  },
  SearchbarStyle: {
    marginTop: 10
  },
  imageCard: {
    marginTop: 22,
    borderRadius: 8,
    marginBottom: 22,
  },
  imageCardHeading: {
    fontSize: 16,
    color: colors.themeColor,
    paddingVertical: 0,
    marginVertical: 0,
  },
  imageCardText: {
    padding: 0,
    margin: 0,
    paddingVertical: 0,
    marginVertical: 0,
  },
  serviceView:{ margin: 20, alignItems: 'center', width: 55 },
  serviceImage:{ width: 35, height: 35 },
  serviceText:{ fontSize: 10, textAlign: 'center', marginTop: 5 },
  flatListView:{ backgroundColor: 'white', marginTop: 20, padding: 10 }
})

export default HomeScreen;
