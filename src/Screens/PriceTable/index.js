import React, { Component } from 'react'
import { StyleSheet, View ,ScrollView} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import { colors } from '../../Common/colors';
import Header from '../../Components/Header'

export default class PriceTableScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Electrical Parts'],
      tableData: [
        ['Spare Parts', 'Prices'],
        ['Non-inverted PCB repaired', '500 (Labour Cost 300)'],
        ['Non-inverted PCB repaired', '500 (Labour Cost 300)'],
        ['Non-inverted PCB repaired', 500],
        ['Non-inverted PCB repaired', 1500]
      ], 
      tableHeadOne: ['General Parts'],
      tableDataOne: [
        ['Spare Parts', 'Prices'],
        ['Non-inverted PCB repaired', '500 (Labour Cost 300)'],
        ['Non-inverted PCB repaired', '500 (Labour Cost 300)'],
        ['Non-inverted PCB repaired', 500],
        ['Non-inverted PCB repaired', 1500]
      ], 
      tableHeadTwo: ['Service Parts'],
      tableDataTwo: [
        ['Spare Parts', 'Prices'],
        ['Non-inverted PCB repaired', '500 (Labour Cost 300)'],
        ['Non-inverted PCB repaired', '500 (Labour Cost 300)'],
        ['Non-inverted PCB repaired', 500],
        ['Non-inverted PCB repaired', 1500]
      ]
    }
  }

  render() {
    const state = this.state;
    return (
      <View style={styles.container}>
        <Header title="Pricing of spare parts" icon={true} />
        <ScrollView>
        <View style={styles.wrapper}>
       <Table borderStyle={{borderWidth: 0.5, borderColor: 'grey',borderRadius:15}}>
          <Row data={state.tableHead} style={styles.head} textStyle={styles.headText}/>
          <Rows data={state.tableData} textStyle={styles.text}/>
        </Table>
       </View>
       <View style={styles.wrapper}>
       <Table borderStyle={{borderWidth: 0.5, borderColor: 'grey',borderRadius:15}}>
          <Row data={state.tableHeadOne} style={styles.head} textStyle={styles.headText}/>
          <Rows data={state.tableDataOne} textStyle={styles.text}/>
        </Table>
       </View>
       <View style={styles.wrapper}>
       <Table borderStyle={{borderWidth: 0.5, borderColor: 'grey',borderRadius:15}}>
          <Row data={state.tableHeadTwo} style={styles.head} textStyle={styles.headText}/>
          <Rows data={state.tableDataTwo} textStyle={styles.text}/>
        </Table>
       </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.background },
  head: { height: 40, backgroundColor: colors.themeColor},
  text: { margin: 6,color:'black',textAlign:'center',fontSize:12},
  headText:{color:'white',marginLeft:10,fontSize:18},
  wrapper:{padding: 16, paddingTop: 30,margin:0,borderRadius:5,backgroundColor:'white',margin:10}
});






















// import React, { useEffect } from 'react'
// import {View ,Text,StyleSheet } from 'react-native'
// import { colors } from '../../Common/colors'
// import { CommonStyles } from '../../Common/CommonStyles'
// import Header from '../../Components/Header'

//  const PriceTableScreen =()=>{

//   useEffect(()=>{

//   })

//   return(
//       <View style={styles.container}>
//       <Header title="Pricing of spare parts" icon={true} />

//       <View style={styles.wrapper}>
//         {/* <View style={styles.head}>
//           <Text style={styles.headerTextStyle}>Electrical Parts</Text>
//         </View> */}
//         <View style={{borderWidth:0.7,borderRadius:5}}>
//         <View style={styles.head}>
//           <Text style={styles.headerTextStyle}>Electrical Parts</Text>
//         </View>
//         <View style={[CommonStyles.RowSpaceAround,CommonStyles.mt10,CommonStyles.mb10]}>
//             <Text>Hello</Text>
//             <View style={{borderRightWidth:0.2,height:20}}></View>

//             <Text>Hello</Text>
//         </View>
    
//         <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', marginTop: 10, marginBottom: 10 }}></View>
//         {/* <View style={[CommonStyles.RowSpaceAround,CommonStyles.mt10,CommonStyles.mb10]}>
//             <Text>Hello</Text>
//             <Text>Hello</Text>
//         </View> */}
//         </View>

//       </View>
//       </View>
//   )
// }
// const styles=StyleSheet.create({
//   container:{
//     flex:1,
//     backgroundColor:colors.background
//   },
//   wrapper:{
//     margin:20
//   },
//   head:{
//     backgroundColor:colors.themeColor,  
//     padding:10,
//     borderRadius:5
//   },
//   headerTextStyle:{
//       color:'white',
//       fontSize:18
//   }
// })
// export default PriceTableScreen