import React, { useEffect } from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Image,TouchableOpacity } from 'react-native'
import { color } from 'react-native-reanimated'
import { colors } from '../../Common/colors'
import Header from '../../Components/Header'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { Colors } from 'react-native/Libraries/NewAppScreen'


const serviceData = [
    { id: 1, name: 'service' },
    { id: 2, name: 'Repair' }, { id: 3, name: 'Installation & Unistallation' },
    { id: 4, name: 'AMC Packages' }
]

const OrderDetailsScreen = (props) => {

    const movetToScreen=()=>{
        props.navigation.navigate('OrderInfoScreen')
      }
    

    return (
        <View style={styles.container}>
        <Header title="Order" icon={true} />
          <ScrollView>
           <View style={{backgroundColor:colors.themeColor,padding:10,flexDirection:'row',marginTop:10}}>
               <Image source={require('../../../assets/images/serviceTwo.png')} style={{height:20,width:20}}  />
               <Text style={{color:'white',marginLeft:10,fontWeight:'bold',fontSize:15}}>Service</Text>
           </View>

          <View style={{backgroundColor:'white',marginTop:5}}>
          <View style={{marginTop:5,backgroundColor:'white',padding:5,flexDirection:'row',justifyContent:'space-between'}}>
           <View style={{backgroundColor:colors.themeColor,marginTop:5,width:'50%',padding:3,borderTopRightRadius:30,elevation:5}}>
               <Text style={{marginLeft:10, color:'white'}}>Offer valid for 10 days</Text>
           </View>
           <View style={{flexDirection:'row',margin:6,marginRight:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.themeColor} style={{margin:4}}  />
               <Text style={{color:colors.themeColor,fontWeight:'bold',fontSize:16}}>899</Text>
           </View>       
           </View>
           <View style={{margin:20,marginTop:5}}>
               <Text style={{color:'black'}}>Standard Plan (2 Services per AC)</Text>
               <Text style={[styles.textStyle,{marginTop:3}]}>. 1 st Service now, 2nd after 3 months</Text>
               <Text style={styles.textStyle}>. Plan valid for 9 months</Text>
               <Text style={styles.textStyle}>. Save extra Rs100 with the plan</Text>

           </View>
           <View style={[styles.rowspace,{margin:15,marginLeft:23}]}>
              <View>
              <Text style={{color:'black'}}>View Details</Text>
              </View>
              <View style={[styles.row,{borderWidth:0.2,padding:5,borderRadius:2}]}>
                  <Text style={{color:colors.themeColor}}>ADD</Text>
                  <AntDesign name="plus" color={colors.themeColor} size={19} style={{marginLeft:5}}  />
              </View>

           </View>
          </View>
          <View style={{backgroundColor:'white',marginTop:5,marginBottom:5}}>
          <View style={{marginTop:5,backgroundColor:'white',padding:5,flexDirection:'row',justifyContent:'space-between'}}>
           <View style={{backgroundColor:colors.themeColor,marginTop:5,width:'50%',padding:3,borderTopRightRadius:30,elevation:5}}>
               <Text style={{marginLeft:10, color:'white'}}>Offer valid for 10 days</Text>
           </View>
           <View style={{flexDirection:'row',margin:6,marginRight:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.themeColor} style={{margin:4}}  />
               <Text style={{color:colors.themeColor,fontWeight:'bold',fontSize:16}}>600</Text>
           </View>       
           </View>
           <View style={{margin:20,marginTop:5}}>
               <Text style={{color:'black'}}>Standard Plan (2 Services per AC)</Text>
               <Text style={[styles.textStyle,{marginTop:3}]}>. 1 st Service now, 2nd after 3 months</Text>
               <Text style={styles.textStyle}>. Plan valid for 9 months</Text>
               <Text style={styles.textStyle}>. Save extra Rs100 with the plan</Text>

           </View>
           <View style={[styles.rowspace,{margin:15,marginLeft:23}]}>
              <View>
              <Text style={{color:'black'}}>View Details</Text>
              </View>
              <View style={[styles.row,{borderWidth:0.2,padding:5,borderRadius:2}]}>
                  <Text style={{color:colors.themeColor}}>ADD</Text>
                  <AntDesign name="plus" color={colors.themeColor} size={19} style={{marginLeft:5}}  />
              </View>

           </View>
          </View>

         <View style={{backgroundColor:'white'}}>
         <View style={[styles.row, styles.textView,{marginTop:10}]}>
          <Image source={require('../../../assets/images/percentage.png')} style={{ height: 30, width: 30, marginTop: 7,marginLeft:15 }} />
           <View>
           <Text style={{ marginLeft: 10, fontSize: 16, marginTop:0,color:'white' }}>30% off on Service & Repair</Text>
          <Text style={{ marginLeft: 12, fontSize: 14, marginTop:0,color:'white' }}>3 offers available</Text>
           </View>
          </View>

          <View style={{margin:30}}>
              <View style={{backgroundColor:colors.themeColor,borderRadius:5,flexDirection:'row',justifyContent:'space-between'}}>
               <Text style={styles.popular}>12 months (Popular plan)</Text>
               <View style={styles.row}>
               <View style={{flexDirection:'row',margin:6,marginRight:10,marginTop:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.white} style={{margin:4,textDecorationLine: 'line-through'}}  />
               <Text style={{color:colors.white,fontWeight:'bold',fontSize:16,textDecorationLine: 'line-through'}}>900</Text>
              </View>
               <View style={{flexDirection:'row',margin:6,marginRight:10,marginTop:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.white} style={{margin:4}}  />
               <Text style={{color:colors.white,fontWeight:'bold',fontSize:16}}>599</Text>
              </View>
               </View>
              </View>

              <View style={{backgroundColor:'#B4D2D7AD',flexDirection:'row',justifyContent:'space-between',marginTop:10,borderRadius:5}}>
               <Text style={styles.popular}>6 months (Popular plan)</Text>
               <View style={styles.row}>
               <View style={{flexDirection:'row',margin:6,marginRight:10,marginTop:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.white} style={{margin:4,textDecorationLine: 'line-through'}}  />
               <Text style={{color:colors.white,fontWeight:'bold',fontSize:16,textDecorationLine: 'line-through'}}>800</Text>
              </View>
               <View style={{flexDirection:'row',margin:6,marginRight:10,marginTop:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.white} style={{margin:4}}  />
               <Text style={{color:colors.white,fontWeight:'bold',fontSize:16}}>499</Text>
              </View>
               </View>
              </View>

              <View style={{backgroundColor:'#B4D2D7AD',flexDirection:'row',justifyContent:'space-between',marginTop:10,borderRadius:5}}>
               <Text style={styles.popular}>3 months (Popular plan)</Text>
               <View style={styles.row}>
               <View style={{flexDirection:'row',margin:6,marginRight:10,marginTop:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.white} style={{margin:4,textDecorationLine: 'line-through'}}  />
               <Text style={{color:colors.white,fontWeight:'bold',fontSize:16,textDecorationLine: 'line-through'}}>600</Text>
              </View>
               <View style={{flexDirection:'row',margin:6,marginRight:10,marginTop:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.white} style={{margin:4}}  />
               <Text style={{color:colors.white,fontWeight:'bold',fontSize:16}}>199</Text>
              </View>
               </View>
              </View>
             
          </View>
         </View>

         <View style={styles.subTotal}>
           <View style={{margin:20}}>
           <View style={styles.rowspace}>
                 <Text>Item Total & Price</Text>
                 <View style={styles.subTotalMainView}>
                  <FontAwesome name="rupee"  size={12} style={{margin:4}}  />
                  <Text style={{fontSize:14,color:'grey'}}>199</Text>
                 </View>
             </View>

             <View style={styles.rowspace}>
                 <Text>Saftey & Suport Fee</Text>
                 <View style={styles.subTotalMainView}>
                  <FontAwesome name="rupee"  size={12} style={{margin:4}}  />
                  <Text style={{fontSize:14,color:'grey'}}>99</Text>
                 </View>
             </View>

             <View style={styles.rowspace}>
                 <Text>Membership Discount</Text>
                 <View style={styles.subTotalMainView}>
                  <FontAwesome name="rupee"  size={12} style={{margin:4}}  />
                  <Text style={{fontSize:14,color:'grey'}}>-100</Text>
                 </View>
             </View>
             <View style={{borderBottomWidth:0.2,borderColor:'grey',marginTop:5}}></View>
             <View style={[styles.rowspace,{marginTop:25,marginLeft:5}]}>
                 <Text style={{fontWeight:'bold'}}>TOTAL</Text>
                 <View style={styles.subTotalMainView}>
                  <FontAwesome name="rupee"  size={12} style={{margin:4}} color={colors.themeColor}  />
                  <Text style={{fontSize:14,color:colors.themeColor}}>1499</Text>
                 </View>
             </View>
           </View>
         </View>

          <TouchableOpacity onPress={()=> movetToScreen()}>
             <View style={{marginBottom:20,margin:20}}>
               <Text style={styles.buttonTextStyle}>Continue</Text>
            </View>
         </TouchableOpacity>
         
        
          
          </ScrollView>
        
          
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background
    },
    rowspace:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    services:{
        margin:20
    },
    textStyle:{
        color:'black'
    },
    row:{
        flexDirection:'row'
    },
    buttonTextStyle:{
        backgroundColor:colors.themeColor,padding:10,color:'white',borderRadius:5,textAlign:'center',fontSize:16
     },
     textView: {
        backgroundColor: colors.themeColor,
        padding: 10
    },
    popular:{
        textAlign:'left',
        padding:10,
        marginLeft:20,
        color:'white',    
    },
    subTotal:{
        backgroundColor:'white',marginTop:15
    },
    subTotalMainView:{
      flexDirection:'row',
      margin:6,marginRight:10,
      marginTop:1
    }


})
export default OrderDetailsScreen 