import React, { useEffect } from 'react'
import {View ,Text,StyleSheet ,TextInput, ScrollView} from 'react-native'
import { colors } from '../../Common/colors'
import Header from '../../Components/Header'
import { Avatar } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';

 const RatingScreen =()=>{

  useEffect(()=>{

  })

  return(
      <View style={styles.container}>
      <Header title="Terms & Conditions" />
   
         <View style={styles.mainView}>
         <ScrollView>
              <View style={styles.AvatarView}>
                  <Avatar.Image size={100} source={{ uri: 'https://www.paychex.com/sites/default/files/styles/1200wide/public/image/2020-09/cares-act-employee-retention-credit.jpg?itok=9llOe7NR' }} />
                  <Text style={styles.textStyle}>Sam David</Text>
              </View>
              <View style={{margin:20}}>
                <View style={{marginTop:20}}>
                    <TextInput
                     placeholder="Change Name"
                     style={styles.textInputStyle}
                    />
                </View>
                <View style={{marginTop:20}}>
                    <TextInput
                     placeholder="Email"
                     style={styles.textInputStyle}
                    />
                </View>
                <View style={{marginTop:20}}>
                    <TextInput
                     placeholder="Mobile Number"
                     style={styles.textInputStyle}
                    />
                </View>
              </View>
              </ScrollView>
             <TouchableOpacity>
             <View style={{marginBottom:20,margin:20}}>
               <Text style={styles.buttonTextStyle}>Save Changes</Text>
             </View>
             </TouchableOpacity>
          </View>
      
       
         
      </View>
  )
}
const styles=StyleSheet.create({
container:{
       flex:1,
       backgroundColor:colors.background
 },
 mainView:{
     flex:3,
     backgroundColor:'white',
     marginTop:'20%',
     borderTopLeftRadius:80,
     borderTopRightRadius:80
 },
 AvatarView:{
     alignItems:'center',marginTop:30
 },
 textStyle:{
    alignItems:'center',fontSize:18,color:'black',marginTop:10
 },
 textInputStyle:{
     borderWidth:0.2,
     borderRadius:2,
     paddingLeft:25,
     
 },
 buttonTextStyle:{
    backgroundColor:colors.themeColor,padding:10,color:'white',borderRadius:5,textAlign:'center',fontSize:16
 }

})
export default RatingScreen