import React  from 'react'
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import { colors } from '../../Common/colors'
import Header from '../../Components/Header'

const TermsConditionsScreen = () => {
    return (
        <View style={styles.container}>
            <Header title="Terms & Conditions" />
            <View style={styles.mainView}>
                <ScrollView>
                    <View style={styles.termsStyle}>
                        <Text style={styles.textStyle} >1. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                        specimen book.</Text> 
                       <Text style={styles.textStyle} >2. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                      specimen book.</Text>
                      <Text style={styles.textStyle} >3. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                      specimen book.</Text>
                      <Text style={styles.textStyle} >4. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                      specimen book.</Text>
                      <Text style={styles.textStyle} >5. Lorem ipsum, or lipsum as it is sometimes known, is dummy
                        text used in laying out print, graphic or web designs.
                        The passage is attributed to an unknown typesetter in the 15th century who is
                        thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type
                      specimen book.</Text>
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background
    },
    mainView: {
        flex: 3,
        backgroundColor: 'white',
        marginTop: '10%',
        borderTopLeftRadius: 80,
        borderTopRightRadius: 80
    },

    textStyle: {
        fontSize: 14, color: 'black', marginTop: 10, textAlign: 'justify'
    },
    termsStyle: {
        margin: 20, marginTop: 40
    }


})
export default TermsConditionsScreen