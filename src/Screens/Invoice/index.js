import React from 'react'
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native'
import { colors } from '../../Common/colors'
import Header from '../../Components/Header'
import { Avatar } from 'react-native-paper'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Entypo from 'react-native-vector-icons/Entypo'
import { CommonStyles } from '../../Common/CommonStyles'
import { TouchableOpacity } from 'react-native-gesture-handler'

const InvoiceScreen = () => {

    return (
        <View style={styles.container}>

            <View style={{ marginBottom: '25%' }}>
                <Header icon={true} />
            </View>

            <View style={styles.wrapper}>
                <View style={{ flexDirection: 'row', justifyContent: "center", position: 'absolute', top: -50, left: '38%' }}>
                    <Avatar.Image size={100} source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRdD6FSR0u2BjRuhMvxry5dsF5HwQcJKvfOw&usqp=CAU' }} />
                </View>

                <View>
                    <Text style={{ marginTop: '15%', textAlign: 'center', color: 'black' }}>Snagam Tirimithiz</Text>

                </View>
                <ScrollView>
                    <View>
                        <View style={styles.OrderNumber}>
                            <Text># Order Number : 25345</Text>
                            <Text style={{ color: colors.themeColor }}>OTP : 2456</Text>
                        </View>

                        <View style={styles.orderDetails}>
                            <Text style={styles.orderDetailsTextStyle}>Order Details</Text>
                        </View>
                    </View>

                    <View>
                        <View style={styles.customerDetails}>
                            <View style={[CommonStyles.row, CommonStyles.mt5]}>
                                <FontAwesome name="phone" color={colors.themeColor} size={15} style={{ width: '10%', marginTop: 3 }} />
                                <Text style={{ width: '90%' }}>+91 9998882255</Text>
                            </View>

                            <View style={[CommonStyles.row, CommonStyles.mt5]}>
                                <MaterialCommunityIcons name="brightness-7" color={colors.themeColor} size={15} style={{ width: '10%', marginTop: 3 }} />
                                <Text style={{ width: '90%' }}>+91 9998882255</Text>
                            </View>

                            <View style={[CommonStyles.row, CommonStyles.mt5]}>
                                <Entypo name="location-pin" color={colors.themeColor} size={15} style={{ width: '10%', marginTop: 3 }} />
                                <Text style={{ width: '90%', textAlign: 'justify' }}>8-3-228  Rahmat nagar , yusufguda check post, Hyderabad.</Text>
                            </View>

                        </View>

                        <View style={styles.orderDetails}>
                            <Text style={styles.orderDetailsTextStyle}>Customer Info</Text>
                        </View>
                    </View>

                    <View>
                        <View style={styles.customerDetails}>
                            <View style={CommonStyles.RowSpaceBetween}>
                                <Text>1 X Washing machine repair</Text>
                                <View style={CommonStyles.row}>
                                    <FontAwesome name="rupee" size={15} color='grey' style={{ margin: 4 }} />
                                    <Text>499</Text>
                                </View>
                            </View>

                            <View style={CommonStyles.RowSpaceBetween}>
                                <Text>Other Chanrges (saftey charges)</Text>
                                <View style={CommonStyles.row}>
                                    <FontAwesome name="rupee" size={15} color='grey' style={{ margin: 4 }} />
                                    <Text>100</Text>
                                </View>
                            </View>

                            <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', marginTop: 10, marginBottom: 10 }}></View>
                            <View style={CommonStyles.RowSpaceBetween}>
                                <Text style={{ color: colors.themeColor, fontWeight: 'bold', fontSize: 16 }}>Total</Text>
                                <View style={CommonStyles.row}>
                                    <FontAwesome name="rupee" size={17} color='grey' style={{ margin: 4 }} color={colors.themeColor} />
                                    <Text style={{ color: colors.themeColor, fontWeight: 'bold', fontSize: 16 }}>699</Text>
                                </View>
                            </View>

                        </View>
                        <View style={styles.orderDetails}>
                            <Text style={styles.orderDetailsTextStyle}>Job Info</Text>
                        </View>
                    </View>

                

                    <TouchableOpacity>
                        <View>
                            <View style={styles.invoice}>
                                <FontAwesome5 name="file-download" size={17} color={colors.themeColor} style={{ margin: 4, marginRight: 10 }} />
                                <Text style={{ color: colors.themeColor, fontSize: 18 }}>Download Invoice</Text>
                            </View>
                        </View>
                    </TouchableOpacity>


                </ScrollView>
            </View>




        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background
    },
    wrapper: {
        flex: 2,
        backgroundColor: 'white',
        borderTopRightRadius: 100,
        borderTopLeftRadius: 100
    },
    OrderNumber: {
        marginTop: '8%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 20,
        borderWidth: 1,
        padding: 20, borderRadius: 5,
        borderColor: 'grey'
    },
    invoice: {
        marginTop: '8%',
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 20,
        borderWidth: 1,
        padding: 15, borderRadius: 5,
        borderColor: colors.themeColor
    },
    customerDetails: {
        marginTop: '8%',
        margin: 20,
        borderWidth: 1,
        padding: 20, borderRadius: 5,
        borderColor: 'grey'
    },
    orderDetails: {
        position: 'absolute',
        top: 10, left: 30
    },
    orderDetailsTextStyle: { backgroundColor: 'white', padding: 10, color: 'black', fontWeight: 'bold' }
})
export default InvoiceScreen