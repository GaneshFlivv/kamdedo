import React, { useEffect } from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Image,TouchableOpacity } from 'react-native'
import { color } from 'react-native-reanimated'
import { colors } from '../../Common/colors'
import Header from '../../Components/Header'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import AntDesign from 'react-native-vector-icons/AntDesign'


const serviceData = [
    { id: 1, name: 'service' },
    { id: 2, name: 'Repair' }, { id: 3, name: 'Installation & Unistallation' },
    { id: 4, name: 'AMC Packages' }
]

const SubDetailScreen = (props) => {

    const movetToScreen=()=>{
        props.navigation.navigate('OrderDetailsScreen')
      }

    return (
        <View style={styles.container}>
        <Header title="Ac Service and Repair" icon={true} />
          <ScrollView>
          <View style={[styles.rowspace,styles.services]}>
           <View style={{borderWidth:1,padding:5,backgroundColor:colors.themeColor,width:90,borderRadius:5,borderColor:colors.themeColor}}>
             <Text style={{color:'white',textAlign:'center'}}>Service</Text>
            </View>
            <View style={{borderWidth:1,padding:5,backgroundColor:colors.themeColor,width:90,borderRadius:5,borderColor:colors.themeColor}}>
             <Text style={{color:'white',textAlign:'center'}}>Repair</Text>
            </View>
            <View style={{borderWidth:1,padding:5,backgroundColor:colors.themeColor,width:90,borderRadius:5,borderColor:colors.themeColor}}>
             <Text style={{color:'white',textAlign:'center'}}>AMC</Text>
            </View>
           </View>

           <View style={{backgroundColor:colors.themeColor,padding:10,flexDirection:'row'}}>
               <Image source={require('../../../assets/images/serviceTwo.png')} style={{height:20,width:20}}  />
               <Text style={{color:'white',marginLeft:10,fontWeight:'bold',fontSize:15}}>Service</Text>
           </View>

          <View style={{backgroundColor:'white',marginTop:5}}>
          <View style={{marginTop:5,backgroundColor:'white',padding:5,flexDirection:'row',justifyContent:'space-between'}}>
           <View style={{backgroundColor:colors.themeColor,marginTop:5,width:'50%',padding:3,borderTopRightRadius:30,elevation:5}}>
               <Text style={{marginLeft:10, color:'white'}}>Offer valid for 10 days</Text>
           </View>
           <View style={{flexDirection:'row',margin:6,marginRight:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.themeColor} style={{margin:4}}  />
               <Text style={{color:colors.themeColor,fontWeight:'bold',fontSize:16}}>899</Text>
           </View>       
           </View>
           <View style={{margin:20,marginTop:5}}>
               <Text style={{color:'black'}}>Standard Plan (2 Services per AC)</Text>
               <Text style={[styles.textStyle,{marginTop:3}]}>. 1 st Service now, 2nd after 3 months</Text>
               <Text style={styles.textStyle}>. Plan valid for 9 months</Text>
               <Text style={styles.textStyle}>. Save extra Rs100 with the plan</Text>

           </View>
           <View style={[styles.rowspace,{margin:15,marginLeft:23}]}>
              <View>
              <Text style={{color:'black'}}>View Details</Text>
              </View>
              <View style={[styles.row,{borderWidth:0.2,padding:5,borderRadius:2}]}>
                  <Text style={{color:colors.themeColor}}>ADD</Text>
                  <AntDesign name="plus" color={colors.themeColor} size={19} style={{marginLeft:5}}  />
              </View>

           </View>
          </View>
          <View style={{backgroundColor:'white',marginTop:5}}>
          <View style={{marginTop:5,backgroundColor:'white',padding:5,flexDirection:'row',justifyContent:'space-between'}}>
           <View style={{backgroundColor:colors.themeColor,marginTop:5,width:'50%',padding:3,borderTopRightRadius:30,elevation:5}}>
               <Text style={{marginLeft:10, color:'white'}}>Offer valid for 10 days</Text>
           </View>
           <View style={{flexDirection:'row',margin:6,marginRight:10}}>
               <FontAwesome name="rupee"  size={15}color={colors.themeColor} style={{margin:4}}  />
               <Text style={{color:colors.themeColor,fontWeight:'bold',fontSize:16}}>600</Text>
           </View>       
           </View>
           <View style={{margin:20,marginTop:5}}>
               <Text style={{color:'black'}}>Standard Plan (2 Services per AC)</Text>
               <Text style={[styles.textStyle,{marginTop:3}]}>. 1 st Service now, 2nd after 3 months</Text>
               <Text style={styles.textStyle}>. Plan valid for 9 months</Text>
               <Text style={styles.textStyle}>. Save extra Rs100 with the plan</Text>

           </View>
           <View style={[styles.rowspace,{margin:15,marginLeft:23}]}>
              <View>
              <Text style={{color:'black'}}>View Details</Text>
              </View>
              <View style={[styles.row,{borderWidth:0.2,padding:5,borderRadius:2}]}>
                  <Text style={{color:colors.themeColor}}>ADD</Text>
                  <AntDesign name="plus" color={colors.themeColor} size={19} style={{marginLeft:5}}  />
              </View>

           </View>
          </View>

           {/* <View style={{backgroundColor:colors.themeColor,padding:10,flexDirection:'row',marginTop:5}}>
               <Image source={require('../../../assets/images/repairone.png')} style={{height:20,width:20}}  />
               <Text style={{color:'white',marginLeft:10,fontWeight:'bold',fontSize:15}}>Repair</Text>
           </View> */}

         
        
          
          </ScrollView>
          <TouchableOpacity onPress={()=> movetToScreen()}>
             <View style={{marginBottom:20,margin:20}}>
               <Text style={styles.buttonTextStyle}>Continue</Text>
             </View>
             </TouchableOpacity>
          
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background
    },
    rowspace:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    services:{
        margin:20
    },
    textStyle:{
        color:'black'
    },
    row:{
        flexDirection:'row'
    },
    buttonTextStyle:{
        backgroundColor:colors.themeColor,padding:10,color:'white',borderRadius:5,textAlign:'center',fontSize:16
     }


})
export default SubDetailScreen