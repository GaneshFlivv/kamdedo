import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, FlatList,TouchableOpacity } from 'react-native';
import { colors } from '../../Common/colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Searchbar } from 'react-native-paper';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { EventsData, ServiceData, VideoData ,DetailsData, CommentData} from '../../Common/index'
import { Card } from 'react-native-paper';
import Header from '../../Components/Header' 
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'



const ServiceDetailScreen = (props) => {

  const data = props.route.params.data
  console.log(data,"props >>>",props);
  const carouselRef = React.createRef();
  const [activeIndex, setActiveIndex]=useState()

  const _renderItem = ({ item, index }) => {
    return (     
      <Card style={styles.imageCard}>
        <Card.Cover source={item.image} />
      </Card>
    );
  };


  const movetToScreen=()=>{
    props.navigation.navigate('SubDetailScreen')
  }

  const moveToExpert=()=>{
    props.navigation.navigate('ExpertAdviceScreen')
  }

  const moveToPriceTable=()=>{
    props.navigation.navigate('PriceTableScreen')
  }

  


  const _renderItemService = ({ item, index }) => {
    return (
     <TouchableOpacity onPress={()=> movetToScreen()}>
        <View style={styles.serviceView}>
        <Image source={item.image} style={styles.serviceImage} resizeMode="contain" />
        <Text style={styles.serviceText}>{item.title}</Text>
      </View>
     </TouchableOpacity>
    );
  };


  const _renderItemComments = ({ item, index }) => {
    return (
      <View style={{backgroundColor:'white',marginTop:10}}>
      <View style={{marginTop:10,flexDirection:'row',}}>
           <View style={{width:'20%'}}>
           <Image source={item.image} style={{height:50,width:50,marginLeft:20,marginTop:5}} resizeMode="contain" />
           </View>
           <View style={{width:'58%',marginTop:10}}>
               <View style={styles.row}>
               <Text style={{fontSize:18,color:'black'}}>{item.title}</Text>
               <View style={[styles.row,{marginLeft:10}]}>
                <Entypo name="star" size={20} color="#f5a614" style={{marginRight:5}}/>
                <Text>3.0</Text>
               </View>
               </View>
                <Text>{item.subTitle}</Text>
                <Text style={{textAlign:'justify',marginTop:10,marginRight:15}}>{item.content}</Text>
           </View>
           <View style={{width:'20%',marginLeft:5,marginTop:20}}>
             <Text>{item.time}</Text>
           </View>
         </View>
      </View>
    );
  };


 const pagination =()=> {
   console.log('calling >>>>');
    return (
        <Pagination
          dotsLength={EventsData.length}
          activeDotIndex={activeIndex}
          containerStyle={{ backgroundColor: colors.background}}
          dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 8,
              backgroundColor:  'rgba(0, 0, 0, 0.75)'
          }}
          inactiveDotStyle={{
              // Define styles for inactive dots here
          }}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
    );
}

  return (
    <View style={styles.container}>
        <Header title={data}  icon={true}/>
      <ScrollView>
       
        <View>
          <View style={{ margin: 15 }}>
            <Carousel
              ref={carouselRef}
              data={VideoData && VideoData}
              renderItem={_renderItem}
              sliderWidth={380}
              itemWidth={380}
              // layout={'default'}
              // // autoplay={true}
              // autoplayDelay={1000}
              // autoplayInterval={200}
              onSnapToItem={(index) => setActiveIndex(index) }
            />
              { pagination ()}
          </View>
        
        </View>
        <View style={[styles.row, styles.textView]}>
          <Image source={require('../../../assets/images/address.png')} style={{ height: 17, width: 17, marginTop: 3,marginLeft:15 }} />
          <Text style={{ marginLeft:15, fontSize: 16, color:'black' }}>Ac Services we did in Banjara Hills.</Text>
        </View>

        <View style={[styles.row, styles.textView,{marginTop:10}]}>
          <Image source={require('../../../assets/images/percentage.png')} style={{ height: 25, width: 25, marginTop: 2,marginLeft:15 }} />
          <Text style={{ marginLeft: 10, fontSize: 18, marginTop:0 }}>30% off on Service & Repair</Text>
        </View>


        <View style={styles.flatListView}>
          <FlatList
            data={DetailsData}
            renderItem={_renderItemService}
            keyExtractor={(item) => item.id}
            numColumns={4}
          />
        </View>

       <TouchableOpacity onPress={()=> moveToExpert()}> 
       <View style={styles.expertsView}>
          <View style={{flexDirection:'row'}}>
          <Image source={require('../../../assets/images/expertShield.png')} style={{height:25,width:25,marginLeft:20}} resizeMode="contain" />
          <Text style={{fontSize:18,marginLeft:10}}>Experts Advice</Text>
          </View>
          <View> 
            <AntDesign name="right" size={25} style={{marginRight:15}} />
          </View>
        </View>
       </TouchableOpacity>

        <View style={styles.expertsView}>
          <View style={{flexDirection:'row'}}>
          <Image source={require('../../../assets/images/expertShield.png')} style={{height:25,width:25,marginLeft:20}} resizeMode="contain" />
          <Text style={{fontSize:18,marginLeft:10}}>Our Expertise</Text>
          </View>
          <View> 
            <AntDesign name="right" size={25} style={{marginRight:15}} />
          </View>
        </View>


        <View style={styles.wrapper}>

          <View >
            <Carousel
              ref={carouselRef}
              data={EventsData && EventsData}
              renderItem={_renderItem}
              sliderWidth={380}
              itemWidth={380}
              layout={'default'}
              autoplay={true}
              autoplayDelay={100}
              autoplayInterval={200}
            />
          </View>
        </View>

        <TouchableOpacity onPress={()=> moveToPriceTable()}>
        <View style={styles.expertsView}>
          <View style={{flexDirection:'row'}}>
          <Image source={require('../../../assets/images/pricing.png')} style={{height:25,width:25,marginLeft:20}} resizeMode="contain" />
          <Text style={{fontSize:18,marginLeft:10}}>Pricing of spare parts</Text>
          </View>
          <View> 
            <AntDesign name="right" size={25} style={{marginRight:15}} />
          </View>
        </View>

        </TouchableOpacity>
      
        <View style={styles.expertsView}>
          <View style={{flexDirection:'row'}}>
          <Image source={require('../../../assets/images/expertShield.png')} style={{height:25,width:25,marginLeft:20}} resizeMode="contain" />
          <Text style={{fontSize:18,marginLeft:10}}>Saftey Rules</Text>
          </View>
          <View> 
            <AntDesign name="right" size={25} style={{marginRight:15}} />
          </View>
        </View>

        <View style={styles.expertsView}>
          <View style={{flexDirection:'row'}}>
          <Image source={require('../../../assets/images/message.png')} style={{height:25,width:25,marginLeft:20,marginTop:5}} resizeMode="contain" />
          <Text style={{fontSize:18,marginLeft:10}}>Customer Review</Text>     
          </View> 
        </View>
        <View style={{borderBottomWidth:0.3,borderColor:'grey'}}> 
         </View>

         <View style={styles.commnetView}>
          <FlatList
            data={CommentData}
            renderItem={_renderItemComments}
            keyExtractor={(item) => item.id}
          />

          <View style={{marginTop:20}}>
            <Text style={styles.viewMoreStyle}>View More</Text>
          </View>
        </View>
      


       
       
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background
  },
  wrapper: {
    margin: 20
  },
  mainView: {
    backgroundColor: colors.themeColor, padding: 20
  },
  row: {
    flexDirection: 'row'
  },
  textView: {
    backgroundColor: 'white',
    padding: 10
  },
  textStyle: {
    color: 'white', marginLeft: 10
  },
  SearchbarStyle: {
    marginTop: 10
  },
  imageCard: {
    marginTop: 22,
    borderRadius: 8,
    marginBottom: 22,
  },
  imageCardHeading: {
    fontSize: 16,
    color: colors.themeColor,
    paddingVertical: 0,
    marginVertical: 0,
  },
  imageCardText: {
    padding: 0,
    margin: 0,
    paddingVertical: 0,
    marginVertical: 0,
  },
  serviceView:{ margin: 20, alignItems: 'center', width: 55 ,alignSelf:'center'},
  serviceImage:{ width: 40, height: 40 },
  serviceText:{ fontSize: 10, textAlign: 'center', marginTop: 5 },
  flatListView:{ backgroundColor: 'white', marginTop: 20, padding: 10 },
  commnetView: { backgroundColor: 'white',  padding: 10 },
  expertsView:{
    backgroundColor:'white',marginTop:15,padding:10,flexDirection:'row',justifyContent:'space-between'
  },
  row:{
    flexDirection:'row'
  },
  viewMoreStyle:{
    textAlign:'center',
    backgroundColor:'green',
    color:'white',
    padding:10,
    marginTop:30,
    marginBottom:10,
    margin:90,
    borderRadius:10
  }
})

export default ServiceDetailScreen;
 