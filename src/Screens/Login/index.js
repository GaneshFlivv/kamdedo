import React from 'react'
import { View, Text, ImageBackground, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'
import { colors } from '../../Common/colors'

const LoginScreen = () => {

  return (
    <View style={styles.container}>
      <ImageBackground source={require('../../../assets/images/backgroundImage.png')} style={styles.image} >
        <View style={styles.imageStyle}>
          <Image source={require('../../../assets/images/logofour.png')} style={{ width: 290, height: 50 }} />
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 25 }}>
          <View style={styles.imageTextStyle} >
            <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUcHvdv9ccfB7BT-X3VOSMUH9GI_pYLe14Xw&usqp=CAU' }} style={{ width: 30, height: 20, marginTop: 13 }} />
            <TextInput
              placeholder="+91"
            />
          </View>
          <View style={{ marginLeft: 5 }}>
            <TextInput
              placeholder="Mobile Number"
              style={styles.textInputStyle}
              keyboardType="numeric"
            />
          </View>
        </View>
        <TouchableOpacity>
          <View style={{ marginBottom: 20, margin: 20 }}>
            <Text style={styles.buttonTextStyle}>Login</Text>
          </View>
        </TouchableOpacity>
      </ImageBackground>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  },
  buttonTextStyle: {
    backgroundColor: colors.themeColor, padding: 10, color: 'white', borderRadius: 5, textAlign: 'center', fontSize: 18
  },
  imageStyle: {
    margin: 20, flexDirection: 'row', justifyContent: 'center', marginTop: '70%'
  },
  imageTextStyle: {
    borderWidth: 0.2, borderRadius: 2, width: 100, paddingLeft: 20, flexDirection: 'row', justifyContent: 'space-between',
  },
  textInputStyle: {
    borderWidth: 0.2, borderRadius: 2, width: 250, paddingLeft: 20
  }

})
export default LoginScreen