import React from 'react'
import { View, Text, ImageBackground, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'
import { colors } from '../../Common/colors'
import AntDesign from 'react-native-vector-icons/AntDesign'

const OtpVerification = () => {

  return (
    <View style={styles.container}>
      <ImageBackground source={require('../../../assets/images/backgroundImage.png')} style={styles.image} >
        <View style={styles.imageStyle}>
          <Image source={require('../../../assets/images/otpScreen.png')} style={{ width: 290, height: 170 }} />
        </View>
        <View>
            <Text style={styles.textStyle}>Enter the OTP which is sent to your</Text>
            <Text style={[styles.textStyle,{marginTop:10}]}>associated account</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 50,marginTop:30 }}>
            <TextInput  
            
              style={styles.textInputStyle}
            />
             <TextInput  
              style={styles.textInputStyle}
            />
             <TextInput  
              style={styles.textInputStyle}
            />
             <TextInput  
              style={styles.textInputStyle}
            />
        </View>
        <View style={{flexDirection:'row',justifyContent:'flex-end',marginRight:20}}>
            <Text style={{fontSize:24,color:colors.themeColor}}>Next</Text>
            <AntDesign name="rightcircle" size={40} style={{marginLeft:15}} color={colors.themeColor} />
        </View>
      
      </ImageBackground>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  },
  buttonTextStyle: {
    backgroundColor: colors.themeColor, padding: 10, color: 'white', borderRadius: 5, textAlign: 'center', fontSize: 18
  },
  imageStyle: {
    margin: 20, flexDirection: 'row', justifyContent: 'center', marginTop: '30%'
  },
  imageTextStyle: {
    borderWidth: 0.2, borderRadius: 2, width: 100, paddingLeft: 20, flexDirection: 'row', justifyContent: 'space-between',
  },
  textInputStyle:{borderBottomWidth:1,width:50},
  textStyle:{textAlign:'center',color:colors.themeColor,fontSize:16}

})
export default OtpVerification 