import React from 'react';
import { View, Image, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { navigationRef } from '../../RootNavigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CartScreen from '../Screens/Cart'
import EditProfileScreen from '../Screens/MyProfile/EditProfile'
import NoInternetScreen from '../Screens/NoInternet'
import TermsConditionsScreen from '../Screens/TermsConditions'
import PrivacyPolicyScreen from '../Screens/PrivacyPolicy'
import ExpertAdviceScreen from '../Screens/ExpertAdvice'
import RatingScreen from '../Screens/Rating'
import SafteyRulesScreen from '../Screens/SafteyRules'
import LoginScreen from '../Screens/Login'

import SidebarMenu from '../Components/SidebarMenu';
// import HomeScreen from '../Screens/Home';
import HomeScreen from '../Screens/Home'
import MyBookingsScreen from '../Screens/MyBookings';
import MyProfileScreen from '../Screens/MyProfile'
import { AuthContext } from '../Common/context';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { StackActions } from '@react-navigation/native';
import { colors } from '../Common/colors';
import OtpVerification from '../Screens/OtpVerification'
import ServiceDetailScreen from '../Screens/ServiceDetails'
import SubDetailScreen from '../Screens/SubDetailScreen'
import OrderDetailsScreen from '../Screens/OrderDetails'
import OrderInfoScreen from '../Screens/OrderInfo'
import PriceTableScreen from '../Screens/PriceTable'
import InvoiceScreen from '../Screens/Invoice'

const AppNavigator = (props) => {
  const Stack = createStackNavigator();
  const Drawer = createDrawerNavigator();
  const RootStack = createStackNavigator();
  const BottomTab = createBottomTabNavigator();

  const handleMoveToback = (navigation) => {
    navigation.dispatch(StackActions.popToTop());
  };




  const NavigationDrawerStructure = (props) => {
    const toggleDrawer = () => {
      props.navigationProps.toggleDrawer();
    };



    return (
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity onPress={toggleDrawer} style={styles.drawerIconStyle}>
          <Image
            source={require('../../assets/images/Sidemenu.png')}
            resizeMode="cover"
            style={styles.Sidemenu}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const HomeScreenStack = ({ navigation }) => {
    return (
      <Stack.Navigator none initialRouteName="HomeScreen">
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ServiceDetailScreen"
          component={ServiceDetailScreen}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="SubDetailScreen"
          component={SubDetailScreen}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="OrderDetailsScreen"
          component={OrderDetailsScreen}
          options={{
            headerShown: false,
          }}
        />

       <Stack.Screen
          name="OrderInfoScreen"
          component={OrderInfoScreen}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="ExpertAdviceScreen"
          component={ExpertAdviceScreen}
          options={{
            headerShown: false,
          }}
        />

         <Stack.Screen
          name="PriceTableScreen"
          component={PriceTableScreen}
          options={{
            headerShown: false,
          }}
        />

          <Stack.Screen
          name="InvoiceScreen"
          component={InvoiceScreen}
          options={{
            headerShown: false,
          }}
        />







      </Stack.Navigator>
    );
  };

  const MyBookingsScreenStack = ({ navigation }) => {
    return (
      <Stack.Navigator none initialRouteName="MyBookingsScreen">
        <Stack.Screen
          name="MyBookingsScreen"
          component={MyBookingsScreen}
          options={{
            headerShown: false
          }}
        />
      </Stack.Navigator>
    );
  };

  const MyProfileScreenStack = ({ navigation }) => {
    return (
      <Stack.Navigator none initialRouteName="MyProfileScreen">
        <Stack.Screen
          name="MyProfileScreen"
          component={MyProfileScreen}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
          name="EditProfileScreen"
          component={EditProfileScreen}
          options={{
            headerShown: false
          }}
        />
          <Stack.Screen
          name="MyBookingsScreen"
          component={MyBookingsScreen}
          options={{
            headerShown: false
          }}
        />

        {/* <Stack.Screen
          name="EditProfileScreen"
          component={EditProfileScreen}
          options={{
            headerShown: false
          }}
        /> */}

        <Stack.Screen
          name="TermsConditionsScreen"
          component={TermsConditionsScreen}
          options={{
            headerShown: false
          }}
        />

        <Stack.Screen
          name="PrivacyPolicyScreen"
          component={PrivacyPolicyScreen}
          options={{
            headerShown: false
          }}
        />


      </Stack.Navigator>
    );
  };


  const CartScreenStack = ({ navigation }) => {
    return (
      <Stack.Navigator none initialRouteName="CartScreen">
        <Stack.Screen
          name="CartScreen"
          component={CartScreen}
          options={{
            // headerLeft: () => (
            //   <NavigationDrawerStructure navigationProps={navigation} />
            // ),
            headerStyle: {
              backgroundColor: 'white', //Set Header color
            },
            headerTintColor: '#fff', //Set Header text color
            headerTitleStyle: {
              //fontWeight: '200', //Set Header text style
              fontSize: 16,
              color: 'gray',
              alignItems: 'center',
            },
          }}
        />
      </Stack.Navigator>
    );
  };

  const SafteyScreenStack = ({ navigation }) => {
    return (
      <Stack.Navigator none initialRouteName="SafteyRulesScreen">
        <Stack.Screen
          name="SafteyRulesScreen"
          component={SafteyRulesScreen}
          options={{
            headerShown: false
          }}
        />
      </Stack.Navigator>
    );
  };



  const DrawerStackScreen = () => {
    return (
      <Drawer.Navigator
        initialRouteName="DashboardScreenStack"
        drawerContent={(props) => <SidebarMenu {...props} />}
        drawerStyle={{
          width: 240,
        }}>
        <Drawer.Screen
          name="DashboardScreenStack"
          options={{
            drawerLabel: () => null,
            title: undefined,
            drawerIcon: () => null,
          }}
          component={DashboardScreenStack}
        />
        <Drawer.Screen
          name="MyBookingsScreen"
          options={{
            drawerLabel: () => null,
            title: undefined,
            drawerIcon: () => null,
          }}
          component={MyBookingsScreenStack}
        />



      </Drawer.Navigator>
    );
  };

  const BottomBarStackScreen = () => {
    return (
      <BottomTab.Navigator
        initialRouteName="DashboardScreenStack"
        //tabBarComponent={(props) => <SidebarMenu {...props} />}
        tabBarOptions={{
          pressColor: 'red',
          style: {
            backgroundColor: '#ffffff',
            alignItems: 'center',
            borderWidth: 0,
            borderTopWidth: 0,
            elevation: 30,
          },
          activeTintColor: '#009E60',
        }}
        
        
        
        >
        <BottomTab.Screen
          name="DashboardScreenStack"
          options={{
            tabBarLabel: () => (
              <View>
                {/* <Text style={styles.bottomLabel}>Home</Text> */}
              </View>
            ),
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
          }}
          component={HomeScreenStack}
        />
        <BottomTab.Screen
          name="MyBookingsScreen"
          options={{
            tabBarLabel: () => (
              <View>
                {/* <Text style={styles.bottomLabel}>Bookings</Text> */}
              </View>
            ),
            tabBarIcon: ({ color, size }) => (
              <FontAwesome
                name="list"
                color={color}
                size={size}
              />
            ),
          }}
          component={MyBookingsScreenStack}
        />

        <BottomTab.Screen
          name="SafteyScreenStack"
          options={{
            tabBarLabel: () => (
              <View>
                {/* <Text style={styles.bottomLabel}>Saftey</Text> */}
              </View>
            ),
            tabBarIcon: ({ color, size }) => (
              <Ionicons
                name="shield-checkmark-sharp"
                color={color}
                size={size}
              />
            ),
          }}
          component={SafteyScreenStack}
        />

        {/* <BottomTab.Screen
          name="CartScreenStack"
          options={{
          
            tabBarIcon: ({ color, size }) => (
              <FontAwesome
                name="shopping-cart"
                color={color}
                size={size}
              />
            ),
          }}
          component={CartScreenStack}
        /> */}




        <BottomTab.Screen
          name="MyProfileScreenStack"
          options={{
            tabBarLabel: () => (
              <View>
                {/* <Text style={styles.bottomLabel}>My Profile</Text> */}
              </View>
            ),
            tabBarIcon: ({ color, size }) => (
              <FontAwesome
                name="user-circle-o"
                color={color}
                size={size}
              />
            ),
          }}
          component={MyProfileScreenStack}
        />




      </BottomTab.Navigator>
    );
  };

  return (
    <AuthContext.Provider>
      <NavigationContainer ref={navigationRef}>
        <RootStack.Navigator>
          <RootStack.Screen
            name="BottomStack"
            component={BottomBarStackScreen}
            options={{
              headerShown: false,
            }}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
};

const styles = StyleSheet.create({
  Sidemenu: {
    marginLeft: 14,
    width: 32,
    height: 25,
  },
  drawerIconStyle: {
    width: 100,
    height: 60,
    paddingTop: 18,
  },
});

export default AppNavigator;
