import Students from '../Common/index';

export function sampleData() {
  return async (dispatch) => {
    const data = Students;
    dispatch(storeSampleData(data));
  };
}

function storeSampleData(sampleTotalData) {
  return {
    type: 'Sample_Students_Data',
    sampleTotalData,
  };
}
