import {combineReducers} from 'redux';
import SampleReducer from '../Reducers/SampleReducer';

export default rootReducer = combineReducers({
  SampleReducer,
});
