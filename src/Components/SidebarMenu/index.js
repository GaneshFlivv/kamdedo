import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import styles from '../../Styles/sidebarMenu';
import {useNavigation} from '@react-navigation/native';
import {CommonActions} from '@react-navigation/native';

const SidebarMenu = (props) => {
  const [routeName, setRouteName] = useState('');
  const navigateScreen = (route) => {
    setRouteName(route);
    props.navigation.navigate(route);
    // navigation.dispatch(
    //   CommonActions.reset({
    //     index: 1,
    //     routes: [
    //       {name: route},
    //       {
    //         name: route,
    //       },
    //     ],
    //   }),
    // );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.UserDetails}>
        <View style={styles.imagemain}>
          <Image
            source={require('../../../assets/images/sampleimage.png')}
            style={styles.userImage}
          />
        </View>
        <View>
          <View>
            <Text style={styles.userName}>Welcome</Text>
            <Text style={styles.userName}>Name</Text>
          </View>
        </View>
      </View>
      <DrawerContentScrollView {...props} showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          style={
            routeName == 'DashboardScreen'
              ? styles.selectedNavbarItem
              : styles.navbarItem
          }
          onPress={() => navigateScreen('DashboardScreen')}>
          <Image
            source={
              routeName == 'DashboardScreen'
                ? require('../../../assets/images/mybookingSelect.png')
                : require('../../../assets/images/mybooking.png')
            }
            style={styles.menuIcon}
          />
          <Text
            style={
              routeName == 'DashboardScreen'
                ? styles.selectedText
                : styles.navbarText
            }>
            Dashboard
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            routeName == 'MyBookingsScreen'
              ? styles.selectedNavbarItem
              : styles.navbarItem
          }
          onPress={() => navigateScreen('MyBookingsScreen')}>
          <Image
            source={
              routeName == 'MyBookingsScreen'
                ? require('../../../assets/images/mybookingSelect.png')
                : require('../../../assets/images/mybooking.png')
            }
            style={styles.menuIcon}
          />
          <Text
            style={
              routeName == 'MyBookingsScreen'
                ? styles.selectedText
                : styles.navbarText
            }>
            My Bokings
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            routeName == 'MembersScreen'
              ? styles.selectedNavbarItem
              : styles.navbarItem
          }
          onPress={() => navigateScreen('MembersScreen')}>
          <Image
            source={
              routeName == 'MembersScreen'
                ? require('../../../assets/images/membersSelect.png')
                : require('../../../assets/images/members.png')
            }
            style={styles.menuIcon}
          />
          <Text
            style={
              routeName == 'MembersScreen'
                ? styles.selectedText
                : styles.navbarText
            }>
            Members
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            routeName == 'SubscriptionsScreen'
              ? styles.selectedNavbarItem
              : styles.navbarItem
          }
          onPress={() => navigateScreen('SubscriptionsScreen')}>
          <Image
            source={
              routeName == 'SubscriptionsScreen'
                ? require('../../../assets/images/subscriptionSelect.png')
                : require('../../../assets/images/subscription.png')
            }
            style={styles.menuIcon}
          />
          <Text
            style={
              routeName == 'SubscriptionsScreen'
                ? styles.selectedText
                : styles.navbarText
            }>
            Subscriptions
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            routeName == 'PackagesScreen'
              ? styles.selectedNavbarItem
              : styles.navbarItem
          }
          onPress={() => navigateScreen('PackagesScreen')}>
          <Image
            source={
              routeName == 'PackagesScreen'
                ? require('../../../assets/images/packagesSelect.png')
                : require('../../../assets/images/packages.png')
            }
            style={styles.menuIcon}
          />
          <Text
            style={
              routeName == 'PackagesScreen'
                ? styles.selectedText
                : styles.navbarText
            }>
            Packages
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            routeName == 'LocationsScreen'
              ? styles.selectedLocationNavbarItem
              : styles.locationnavbarItem
          }
          onPress={() => navigateScreen('LocationsScreen')}>
          <Image
            source={
              routeName == 'LocationsScreen'
                ? require('../../../assets/images/locationSelect.png')
                : require('../../../assets/images/location.png')
            }
            style={styles.locationIcon}
          />
          <Text
            style={
              routeName == 'LocationsScreen'
                ? styles.selectedText
                : styles.navbarText
            }>
            Locations
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            routeName == 'MySettingsScreen'
              ? styles.selectedNavbarItem
              : styles.navbarItem
          }
          onPress={() => navigateScreen('MySettingsScreen')}>
          <Image
            source={
              routeName == 'MySettingsScreen'
                ? require('../../../assets/images/settingsSelect.png')
                : require('../../../assets/images/settings.png')
            }
            style={styles.SettingsmenuIcon}
          />
          <Text
            style={
              routeName == 'MySettingsScreen'
                ? styles.selectedText
                : styles.navbarText
            }>
            My Settings
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            routeName == 'MyProfileScreen'
              ? styles.selectedNavbarItem
              : styles.navbarItem
          }
          onPress={() => navigateScreen('MyProfileScreen')}>
          <Image
            source={
              routeName == 'MyProfileScreen'
                ? require('../../../assets/images/profileSelect.png')
                : require('../../../assets/images/profile.png')
            }
            style={styles.menuIcon}
          />
          <Text
            style={
              routeName == 'MyProfileScreen'
                ? styles.selectedText
                : styles.navbarText
            }>
            My Profile
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            routeName == 'LogoutScreen'
              ? styles.selectedNavbarItem
              : styles.navbarItem
          }
          onPress={() => navigateScreen('LogoutScreen')}>
          <Image
            source={
              routeName == 'LogoutScreen'
                ? require('../../../assets/images/logoutSelect.png')
                : require('../../../assets/images/logout.png')
            }
            style={styles.menuIcon}
          />
          <Text
            style={
              routeName == 'LogoutScreen'
                ? styles.selectedText
                : styles.navbarText
            }>
            Logout
          </Text>
        </TouchableOpacity>
      </DrawerContentScrollView>
      <View style={styles.footerMain}>
        <Image
          source={require('../../../assets/images/appIcon.png')}
          style={styles.footerImage}
          resizeMode="contain"
        />
      </View>
    </SafeAreaView>
  );
};

export default SidebarMenu;
