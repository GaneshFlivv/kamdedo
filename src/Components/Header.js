import React  from 'react'
import {View ,Text ,StyleSheet, TouchableOpacity} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from '@react-navigation/native';


 const Header =(props)=>{

    const navigation = useNavigation();
    const handleMoveGoback = () => {
      navigation.goBack('');
    };

return(
      <View style={styles.container}>
       {props.icon ? 
       (
        <View style={{margin:10,marginTop:15}}>
        <TouchableOpacity   onPress={() => handleMoveGoback()}>
               <MaterialIcons name="arrow-back-ios" size={25}  color="black"  style={{marginLeft:20}}/>
        </TouchableOpacity>
        </View>
       ):(
        <View style={{marginLeft:30}}></View>
    )}
       <View style={styles.textStyleView}>
           <Text style={styles.textStyle}>{props.title}</Text>
       </View>
      </View>
  )
}
const styles=StyleSheet.create({
     container:{
         flexDirection:'row',
         marginTop:5,
         marginBottom:5
     },
     textStyleView:{
         marginTop:10
     },
     textStyle:{
         fontSize:20,
         color:'black'
     }
})
export default Header



