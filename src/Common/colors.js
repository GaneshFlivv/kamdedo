export const colors = {
  themeColor: '#009E60',
  white: '#ffffff',
  themeHeader: '#00B355',
  cardBg: '#EDFFEE',
  // background:'#F2F5FA',
  background: '#E5E5E5',
  grey: '#6b6a64',
  success: 'green',
  failed: 'red',
  warning: 'orange',
};
