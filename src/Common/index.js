


export const EventsData = [
  {
    id: 1,
    title: 'Horse Riding',
    image: require('../../assets/images/Rectangle.png'),
    Duration: '300 days',
    Amount: 200,
  },
  {
    id: 2,
    title: 'Equestrian Arts',
    image: require('../../assets/images/Rectangle.png'),
    Duration: '306 days',
    Amount: 200,
  },
  {
    id: 3,
    title: 'Horse Riding',
    image: require('../../assets/images/Rectangle.png'),
    Duration: '386 days',
    Amount: 200,
  },
  {
    id: 4,
    title: 'Horse Riding',
    image: require('../../assets/images/Rectangle.png'),
    Duration: '36 days',
    Amount: 200,
  },
];



export const VideoData = [
  {
    id: 1,
    title: 'Horse Riding',
    image: require('../../assets/images/videoimg.png'),
    Duration: '300 days',
    Amount: 200,
  },
  {
    id: 2,
    title: 'Equestrian Arts',
    image: require('../../assets/images/videoimg.png'),
    Duration: '306 days',
    Amount: 200,
  },
  {
    id: 3,
    title: 'Horse Riding',
    image: require('../../assets/images/videoimg.png'),
    Duration: '386 days',
    Amount: 200,
  },
  {
    id: 4,
    title: 'Horse Riding',
    image: require('../../assets/images/videoimg.png'),
    Duration: '36 days',
    Amount: 200,
  },
];


export const CommentData = [
  {
    id: 1,
    title: 'Sam David',
    subTitle:'Wed, 24 March, at 1:30 pm',
    image: require('../../assets/images/imageComment.png'),
    Duration: '300 days',
    time:'10:00 pm',
    content:'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs',
    Amount: 200,
  },
  {
    id: 2,
    title: 'Sam David',
    subTitle:'Wed, 24 March, at 1:30 pm',
    image: require('../../assets/images/imageComment.png'),
    Duration: '306 days',
    time:'10:00 pm',
    content:'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs',
    Amount: 200,
  },
 
];



export const DetailsData = [
  {
    id: 1,
    title: 'Service',
    image: require('../../assets/images/serviceone.png'),
 
  },
  {
    id: 2,
    title: 'Repair',
    image: require('../../assets/images/repair.png'),
   
  },
  {
    id: 3,
    title: 'Installation & Uninstallation',
    image: require('../../assets/images/installation.png'),
   
  },
  {
    id: 4,
    title: 'AMC packages',
    image: require('../../assets/images/amc.png'),
 
  },
  
];





export const ServiceData = [
  {
    id: 1,
    title: 'Washing Machine',
    image: require('../../assets/images/machine.png'),
 
  },
  {
    id: 2,
    title: 'Refrigrator',
    image: require('../../assets/images/refridge.png'),
   
  },
  {
    id: 3,
    title: 'Microwave Oven',
    image: require('../../assets/images/gpo.png'),
   
  },
  {
    id: 4,
    title: 'Air Conditioning',
    image: require('../../assets/images/aricondtioning.png'),
 
  },
  {
    id: 5,
    title: 'TV',
    image: require('../../assets/images/tv.png'),
   
  },
  {
    id: 6,
    title: 'Water Purifier',
    image: require('../../assets/images/water.png'),
  
  },
  {
    id: 7,
    title: 'Water Geyser',
    image: require('../../assets/images/waterGeyser.png'),
  
  },
  {
    id: 8,
    title: 'Chimney',
    image: require('../../assets/images/chimney.png'),
   
  },
];

export const BookingsData = [
  {
    id: 1,
    title: 'Sangam Tirimurthy',
    subTitle:'Wed, 24 March, at 1:30 pm',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4lT6c3OEoasZOhpZmzNUMBuE73eW7TBuCgg&usqp=CAU',
    Duration: '300 days',
    Status:'Completed',
    OrderNo:'# Order No: 254356',
    ServiceName: 'Washing Machine Service',
  },
  {
    id: 2,
    title: 'Sangam Tirimurthy',
    subTitle:'Wed, 24 March, at 1:30 pm',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4lT6c3OEoasZOhpZmzNUMBuE73eW7TBuCgg&usqp=CAU',
    Duration: '300 days',
    Status:'Completed',
    OrderNo:'# Order No: 254356',
    ServiceName: 'Washing Machine Service',
  },
  {
    id: 3,
    title: 'Sangam Tirimurthy',
    subTitle:'Wed, 24 March, at 1:30 pm',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4lT6c3OEoasZOhpZmzNUMBuE73eW7TBuCgg&usqp=CAU',
    Duration: '300 days',
    Status:'Completed',
    OrderNo:'# Order No: 254356',
    ServiceName: 'Washing Machine Service',
  },
  {
    id: 4,
    title: 'Sangam Tirimurthy',
    subTitle:'Wed, 24 March, at 1:30 pm',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4lT6c3OEoasZOhpZmzNUMBuE73eW7TBuCgg&usqp=CAU',
    Duration: '300 days',
    Status:'Completed',
    OrderNo:'# Order No: 254356',
    ServiceName: 'Washing Machine Service',
  },
  {
    id: 5,
    title: 'Sangam Tirimurthy',
    subTitle:'Wed, 24 March, at 1:30 pm',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4lT6c3OEoasZOhpZmzNUMBuE73eW7TBuCgg&usqp=CAU',
    Duration: '300 days',
    Status:'Completed',
    OrderNo:'# Order No: 254356',
    ServiceName: 'Washing Machine Service',
  },
  {
    id: 6,
    title: 'Sangam Tirimurthy',
    subTitle:'Wed, 24 March, at 1:30 pm',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4lT6c3OEoasZOhpZmzNUMBuE73eW7TBuCgg&usqp=CAU',
    Duration: '300 days',
    Status:'Completed',
    OrderNo:'# Order No: 254356',
    ServiceName: 'Washing Machine Service',
  },
];


export const TimingsData = [
  {
    id: 1,
    value:'05:30 pm'
  },
  {
    id: 2,
    value:'04:30 pm'
   
  },
  {
    id: 3,
    value:'04:30 pm'
  },
  {
    id: 4,
    value:'04:30 pm'

  },
  {
    id: 5,
    value:'05:30 pm'
  },
  {
    id: 6,
    value:'04:30 pm'
   
  },
  {
    id: 7,
    value:'04:30 pm'
  },
  {
    id: 8,
    value:'04:30 pm'

  },
  {
    id: 9,
    value:'04:30 pm'

  },
  {
    id: 10,
    value:'05:30 pm'
  },
  {
    id: 11,
    value:'04:30 pm'
   
  },
  // {
  //   id: 12,
  //   value:'04:30 pm'
  // },
  // {
  //   id: 13,
  //   value:'04:30 pm'

  // },
];




